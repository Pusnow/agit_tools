javascript:
var notes = document.getElementsByClassName('note-list-page__row');
for (var i = 0; i < notes.length; ++i) {
    let filename = notes[i].outerText.replace(/[<>:\"\/\\|?*\s]+/g, ' ') + '.html';
    let url = '/api/groups' + notes[i].children[0].getAttribute('href').slice(2).replace(/\/notes\//g, '/wikis/');
    console.log('Downloading', filename);
    var headers = new Headers();
    headers.set('Authorization', 'Bearer ' + window.initialState.user.uToken);
    let response = fetch(url, { 'headers': headers }).then(function (response) {
        return response.json();
    }).then(
        function (jsonBody) {
            let htmlBody = jsonBody.wiki.content;
            var link = document.createElement('a');
            link.download = filename;
            var blob = new Blob([htmlBody], { type: 'text/plain' });
            link.href = window.URL.createObjectURL(blob);
            link.click();
            console.log('Download Finish', filename);
        });
}